package com.example.retrofittrainning

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat

class ExtratoAdapter(): RecyclerView.Adapter<ExtratoAdapter.ExtratoHolder>() {

    private var listInflada : List<ExtratosModel> = arrayListOf()


    fun extratoAdapter(listInflada : List<ExtratosModel>){

        this.listInflada = listInflada
        notifyDataSetChanged()
    }

    fun extratoClear(){
        listInflada = emptyList()

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExtratoHolder {

        val extratoView = LayoutInflater.from(parent.context).inflate(R.layout.extratoitem, parent, false)
        return ExtratoHolder(extratoView)
    }

    override fun onBindViewHolder(holder: ExtratoHolder, position: Int) {
        var extrato = listInflada.get(position)
        holder.bind(extrato)
    }

    override fun getItemCount(): Int {
        return listInflada.count()
    }
    inner class ExtratoHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var descricao: TextView = itemView.findViewById(R.id.descricao)
        var valor: TextView = itemView.findViewById(R.id.valor)
        var data: TextView = itemView.findViewById(R.id.data)
        var tipo: TextView = itemView.findViewById(R.id.tipo)

        fun bind(extrato: ExtratosModel) {
            val formatter = SimpleDateFormat("dd/MM/yyyy")

            descricao.text = extrato.description
            valor.text = formatValue(extrato.value)
            data.text = formatter.format(extrato.data)
            data.setTextColor(Color.LTGRAY)
            tipo.setTextColor(Color.LTGRAY)
            if (extrato.value > 0){
                tipo.text = "Recebimento"
                valor.setTextColor(Color.GREEN)
            } else{
                tipo.text = "Pagamento"
                valor.setTextColor(Color.RED)
            }
        }
    }

    private fun formatValue(value: Float): String {
        if (value > 0) {
            val strValue = value.toString()
            if (strValue[strValue.lastIndex-1] == '.'){
                return "R$$value".replace(".",",") + "0"
            }
            else{
                return "R$$value".replace(".",",")
            }
        }
        else {
            val strValue = value.toString()
            if (strValue[strValue.lastIndex-1] == '.'){
                return strValue[0] + "R$" + strValue.substring(1, strValue.lastIndex+1).replace(".",",") + "0"
            }
            return strValue[0] + "R$" + strValue.substring(1, strValue.lastIndex+1).replace(".",",")
        }
    }
}