package com.example.retrofittrainning

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import java.util.concurrent.Executor


class MainActivity : AppCompatActivity() {
   private lateinit var button: Button
    lateinit var userTxt: EditText
    lateinit var passTxt: EditText
    private lateinit var errorMessage : TextView
    private lateinit var username : String
    private lateinit var password: String
    private lateinit var loginHere : Login
    private lateinit var storeLogin : StoreInfo
    private lateinit var progBar : ProgressBar



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userTxt = findViewById(R.id.usernameText)
        passTxt = findViewById(R.id.userPassword)
        errorMessage = findViewById(R.id.loginError)
        button = findViewById(R.id.buttonLogin)
        progBar = findViewById(R.id.progressBar)
        loginHere = Login(this)
        storeLogin = StoreInfo(this)
        button.setOnClickListener{
            if(progBar.isVisible == false){

            progBar.isVisible = true
            username = userTxt.text.toString()
            password = passTxt.text.toString()

            val emailLogin = Patterns.EMAIL_ADDRESS.matcher(username).matches()
            val upperCase = password.contains(Regex("[A-Z]"))
            val specialCharacter = password.contains(Regex("[@#$%^/&+=]"))
            val alphaNumeric = password.contains(Regex("[a-zA-Z0-9]"))

            loginHere.logou.value = null
            errorMessage.isVisible = false
            if(emailLogin&&specialCharacter&&alphaNumeric){
                loginHere.login(username, password)
                observe()
            } else if(emailLogin){
                if(passTxt.text.toString() == ""){
                    passTxt.setError("Campo senha vazio")
                    progBar.isVisible = false
                } else
                {
                    errorMessage.text = "Login ou senha invalidos"
                    errorMessage.isVisible = true
                    progBar.isVisible = false
                }
            } else{
                userTxt.setError("Login Invalido")
                passTxt.setText("")
                progBar.isVisible = false
            }
            }
        }
        verifyUsername()
        if(FingerprintHelper.isAuthenticationAvailable(this)){
            showAuthentication()
        }
    }
    private fun observe(){
        loginHere.logado.observe(this, Observer {

            when(it){
          1 ->{
              progBar.isVisible = false
              goExtratos()
             }
                2 ->{
                    errorMessage.text = "Login ou senha invalidos"
                    errorMessage.isVisible = true
                    progBar.isVisible = false
                }
                3 ->{
                    errorMessage.text = "Sem resposta"
                    errorMessage.isVisible = true
                    progBar.isVisible = false
                }
            }
          })

    }
    private fun goExtratos() {
        var telaExtratos = Intent(this, ExtratosView::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(telaExtratos)
        finishAffinity()
    }

    private fun verifyUsername(){
        if(storeLogin.get("username") != null || storeLogin.get("username") != ""){
            userTxt.setText(storeLogin.get("username"))
        }
    }

    private fun showAuthentication(){
        val executor: Executor = ContextCompat.getMainExecutor(this)

        val biometricPrompt = BiometricPrompt(this@MainActivity,
            executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                }
        })

        val info : BiometricPrompt.PromptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Logar com Digital")
            .setSubtitle("Coloque o dedo na tela para realizar o login")
            //.setDescription("Descricao")
            .setNegativeButtonText("Cancelar")
            .build()

        biometricPrompt.authenticate(info)
    }

}