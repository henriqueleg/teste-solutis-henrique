package com.example.retrofittrainning

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class ExtratosView : AppCompatActivity() {

    private lateinit var extratosGet : ExtratosGet
    private lateinit var storeInfo : StoreInfo
    private lateinit var button: ImageView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extratos_view)

        storeInfo = StoreInfo(this)
        extratosGet = ExtratosGet(this)
        var userName : TextView = findViewById(R.id.nomeUser)
        var userBalance : TextView = findViewById(R.id.saldoUser)
        var userCpf : TextView = findViewById(R.id.cpfUser)
        var nomePessoa : String = storeInfo.get("user_name")
        var cpfPessoa : String = storeInfo.get("user_cpf")
        var balancePessoa : String = storeInfo.get("user_balance")
        storeInfo.remove("user_name")
        storeInfo.remove("user_cpf")
        storeInfo.remove("user_balance")
        extratosGet.pegaExtrato(storeInfo.get("user_token"))

        userName.text = (nomePessoa)
        userBalance.text = (formatValue(balancePessoa.toFloat()))
        userCpf.text = (cpfCnpjMask(cpfPessoa))

        var recycle = findViewById<RecyclerView>(R.id.recyclerView)
        recycle.layoutManager = LinearLayoutManager(this)
        recycle.adapter = extratosGet.adapter
        extratosGet.adapter.extratoAdapter(extratosGet.listExtratos)

        button = findViewById(R.id.logoutButton)
        button.setOnClickListener{
            var alert = AlertDialog.Builder(this)
                .setTitle("Deseja realmente sair?")
                .setPositiveButton("Sair", null)
                .setNegativeButton("Ficar", null)
                .setIcon(R.drawable.ic_baseline_login_24)
                .show()

            var positiveButton : Button = alert.getButton(AlertDialog.BUTTON_POSITIVE)
            var negativeButton : Button = alert.getButton(AlertDialog.BUTTON_NEGATIVE)

            positiveButton.setOnClickListener {
                goBack()
            }

            negativeButton.setOnClickListener {
                alert.hide()
            }
        }
        var swipe = findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)
        swipe.setOnRefreshListener {
            extratosGet.adapter.extratoClear()
            extratosGet.pegaExtrato(storeInfo.get("user_token"))
            swipe.isRefreshing = false
        }
    }

    private fun goBack() {
        var telaLogin = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(telaLogin)
        finishAffinity()
    }

    private fun cpfCnpjMask(cpf: String): String {
        var maskCpf = ""
        var mask = ""

        if(cpf.count() == 11) mask = "###.###.###-##"
        else if(cpf.count() == 14) mask = "##.###.###/####-##"

        var i = 0
        for (m in mask.toCharArray()) {
            if (m != '#') {
                maskCpf += m
                continue
            }
            try {
                maskCpf += cpf[i]
            } catch (e: Exception) {
                break
            }
            i++
        }
        return maskCpf
    }
    private fun formatValue(value: Float): String {
        if (value > 0) {
            val strValue = value.toString()
            if (strValue[strValue.lastIndex-1] == '.'){
                return "R$$value".replace(".",",") + "0"
            }
            else{
                return "R$$value".replace(".",",")
            }
        }
        else {
            val strValue = value.toString()
            if (strValue[strValue.lastIndex-1] == '.'){
                return strValue[0] + "R$" + strValue.substring(1, strValue.lastIndex+1).replace(".",",") + "0"
            }
            return strValue[0] + "R$" + strValue.substring(1, strValue.lastIndex+1).replace(".",",")
        }
    }
}