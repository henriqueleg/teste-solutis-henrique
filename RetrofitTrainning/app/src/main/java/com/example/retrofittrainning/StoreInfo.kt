package com.example.retrofittrainning

import android.content.Context
import android.content.SharedPreferences

class StoreInfo(context: Context) {

    private val preferencias: SharedPreferences =
        context.getSharedPreferences("taskShared", Context.MODE_PRIVATE)

    fun store(key: String, value: String) {
        preferencias.edit().putString(key, value).apply()
    }

    fun remove(key: String) {
        preferencias.edit().remove(key).apply()
    }

    fun get(key: String): String {
        return preferencias.getString(key, "") ?: ""
    }
}