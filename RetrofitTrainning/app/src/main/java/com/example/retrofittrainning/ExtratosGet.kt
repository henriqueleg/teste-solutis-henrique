package com.example.retrofittrainning

import android.content.Context
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ExtratosGet(val context: Context){

    var listExtratos : List<ExtratosModel> = arrayListOf()
    var adapter = ExtratoAdapter()


    fun pegaExtrato(token:String){

        val mRemote = Retrofit.createService(API::class.java)
        val call : Call<List<ExtratosModel>> = mRemote.extratos(token)

        call.enqueue(object : Callback<List<ExtratosModel>>{
            override fun onResponse(call: Call<List<ExtratosModel>>, response: Response<List<ExtratosModel>>)
            {
                if(response.code() != 200){

                } else{

                    listExtratos = response.body()!!
                    adapter.extratoAdapter(listExtratos)
                }
            }

            override fun onFailure(call: Call<List<ExtratosModel>>, t: Throwable) {
                Toast.makeText(context, "Falha ao conseguir extratos", Toast.LENGTH_SHORT).show()
            }
        })
    }
}