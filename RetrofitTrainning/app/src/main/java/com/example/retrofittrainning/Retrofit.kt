package com.example.retrofittrainning
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


class Retrofit {

    companion object {
        private val url = "https://api.mobile.test.solutis.xyz/"
        private lateinit var retrofit: Retrofit

    private fun getRetrofitInstance(): Retrofit {
        val httpClient = OkHttpClient.Builder()

        if (!Companion::retrofit.isInitialized) {
        retrofit = Retrofit.Builder()
                .baseUrl(url)
                .client(httpClient.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit
    }
    fun <S> createService(serviceClass: Class<S>): S {
        return getRetrofitInstance().create(serviceClass)

    }
  }
}