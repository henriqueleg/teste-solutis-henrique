package com.example.retrofittrainning

import retrofit2.http.POST
import retrofit2.Call
import retrofit2.http.*

interface API {
    @POST("login")
    fun login(@Body body: HashMap<String, String>): Call<LoginModel>

    @GET("extrato")
    fun extratos(@Header("token") token: String): Call<List<ExtratosModel>>
}