package com.example.retrofittrainning
import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login (val context: Context){

    val store = StoreInfo(context)
    var nome = ""
    val logou = MutableLiveData<Int>()
    var logado: LiveData<Int> = logou


    fun login(username: String, password: String){
        val login = HashMap<String, String>()
        login["username"] = username
        login["password"] = password


        val mRemote = Retrofit.createService(API::class.java)
        val call: Call<LoginModel> = mRemote.login(login)


        call.enqueue(object : Callback<LoginModel> {
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                if (response.code() != 200) {
                    logou.value = 2

                } else {
                    val values = response.body()
                    store.store("user_name",values!!.name)
                    store.store("user_cpf",values!!.cpf)
                    store.store("user_balance",values!!.balance.toString())
                    store.store("user_token",values!!.token)
                    store.store("username", username)
                    nome = store.get("user_name")
                    logou.value = 1
                }
            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                logou.value = 0
            }
        })
    }



}